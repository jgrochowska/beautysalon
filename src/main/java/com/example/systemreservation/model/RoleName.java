package com.example.systemreservation.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
