package com.example.systemreservation.controller;

import com.example.systemreservation.model.SalonService;
import com.example.systemreservation.repository.SalonServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController

//@PreAuthorize("hasRole('USER')")
@RequestMapping("/api/customer")
public class CustomerController {

    @Autowired
    private SalonServiceRepository salonServiceRepository;

    @GetMapping("/products")
    public List<SalonService> getAllSalonServices(){
        return salonServiceRepository.findAll();
    }
}
