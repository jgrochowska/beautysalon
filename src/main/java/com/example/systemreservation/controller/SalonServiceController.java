package com.example.systemreservation.controller;

import com.example.systemreservation.exception.ResourceNotFoundException;
import com.example.systemreservation.model.SalonService;
import com.example.systemreservation.model.User;
import com.example.systemreservation.repository.SalonServiceRepository;
import com.example.systemreservation.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/manage")
@PreAuthorize("hasRole('ADMIN')")
public class SalonServiceController {

    @Autowired
    private SalonServiceRepository salonServiceRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/services")
    public List<SalonService> getAllSalonServices(){
        return salonServiceRepository.findAll();
    }

    @GetMapping("/services/{id}")
    public ResponseEntity<SalonService> getSalonServiceById(@PathVariable(value = "id") Long salonServiceId) throws ResourceNotFoundException {
        SalonService salonService = salonServiceRepository.findById(salonServiceId)
                .orElseThrow(()-> new ResourceNotFoundException("Nie znaleziono usługi dla id :: " + salonServiceId));
        return ResponseEntity.ok().body(salonService);
    }

    @PostMapping("/services")
    public SalonService createSalonService(@RequestBody SalonService salonService){
        return salonServiceRepository.save(salonService);
    }

    @PutMapping("/services/{id}")
    public ResponseEntity<SalonService> updateSalonService(@PathVariable(value = "id") Long salonServiceId,
                                        @RequestBody SalonService salonServiceDetails) throws ResourceNotFoundException {
        SalonService salonService = salonServiceRepository.findById(salonServiceId)
                .orElseThrow(()-> new ResourceNotFoundException("Nie znaleziono usługi dla id :: " + salonServiceId));

        salonService.setName(salonServiceDetails.getName());
        salonService.setPrice(salonServiceDetails.getPrice());
        //salonService.setDuration(salonServiceDetails.getDuration());
        salonService.setDescription(salonServiceDetails.getDescription());

        final SalonService updateSalonService = salonServiceRepository.save(salonService);
        return ResponseEntity.ok(updateSalonService);
    }

    @DeleteMapping("/services/{id}")
    public Map<String, Boolean> deleteSalonService(@PathVariable(value = "id") Long salonServiceId) throws ResourceNotFoundException {
        SalonService salonService = salonServiceRepository.findById(salonServiceId)
                .orElseThrow(()-> new ResourceNotFoundException("Nie znaleziono usługi dla id :: " + salonServiceId));

        salonServiceRepository.delete(salonService);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }

    @GetMapping("/users")
    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<User> updateUser(@PathVariable(value = "id") Long userId,
                                                           @Valid @RequestBody User userDetails) throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(()-> new ResourceNotFoundException("Nie znaleziono użytkownika dla id :: " + userId));

        user.setName(userDetails.getName());
        user.setEmail(userDetails.getEmail());
        user.setUsername(userDetails.getUsername());
        user.setRoles(userDetails.getRoles());
        user.setPassword(userDetails.getPassword());

        final User updateUser = userRepository.save(user);
        return ResponseEntity.ok(updateUser);
    }

    @PostMapping("/users")
    public User createUser(@RequestBody User user){
        return userRepository.save(user);
    }

    @DeleteMapping("/users/{id}")
    public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId) throws ResourceNotFoundException {
        User user = userRepository.findById(userId)
                .orElseThrow(()-> new ResourceNotFoundException("Nie znaleziono użytkownika dla id :: " + userId));

        userRepository.delete(user);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
