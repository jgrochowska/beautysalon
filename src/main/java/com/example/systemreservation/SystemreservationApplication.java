package com.example.systemreservation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SystemreservationApplication {

    public static void main(String[] args) {
        SpringApplication.run(SystemreservationApplication.class, args);
    }

}
