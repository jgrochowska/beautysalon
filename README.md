# System Reservation
System reservation with admin management panel for beauty salon

## Technologies
* Java 8
* Spring Boot
* Spring Security
* JWT
* PostgreSQL
* REST API
* Angular 8
* Bootstrap 4
* angular-calendar
##### Homepage
![home](./screenshot/s1.png)
##### All services list
![products](./screenshot/s2.png)
##### Login and register page
![login](./screenshot/s4.png)
![register](./screenshot/s5.png)
![register](./screenshot/s6.png)
##### Manage panel
![manage](./screenshot/s3.png)
![manage](./screenshot/s7.png)
![services](./screenshot/s8.png)
![add](./screenshot/s9.png)
![update](./screenshot/s10.png)
##### Calendar for employee
![manage](./screenshot/s12.png)

## TO DO
* Add reservation visits for customers
* Add calendar module for employee
* Add contact page with the ability to send messages

