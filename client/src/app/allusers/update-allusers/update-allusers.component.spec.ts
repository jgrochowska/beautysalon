import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateAllusersComponent } from './update-allusers.component';

describe('UpdateAllusersComponent', () => {
  let component: UpdateAllusersComponent;
  let fixture: ComponentFixture<UpdateAllusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateAllusersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateAllusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
