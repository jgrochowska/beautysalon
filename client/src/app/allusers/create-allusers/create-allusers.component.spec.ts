import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateAllusersComponent } from './create-allusers.component';

describe('CreateAllusersComponent', () => {
  let component: CreateAllusersComponent;
  let fixture: ComponentFixture<CreateAllusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateAllusersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAllusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
