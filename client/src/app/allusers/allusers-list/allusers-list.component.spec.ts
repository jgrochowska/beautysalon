import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllusersListComponent } from './allusers-list.component';

describe('AllusersListComponent', () => {
  let component: AllusersListComponent;
  let fixture: ComponentFixture<AllusersListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllusersListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllusersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
