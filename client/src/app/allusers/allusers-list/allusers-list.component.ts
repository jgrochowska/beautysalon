import { Component, OnInit, PipeTransform  } from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { FormControl } from '@angular/forms';
import {Observable} from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {Router} from '@angular/router';
import {TokenStorageService} from '../../auth/token-storage.service';
import {AllusersService} from '../allusers.service';
import {Allusers} from '../allusers';

@Component({
  selector: 'app-allusers-list',
  templateUrl: './allusers-list.component.html',
  styleUrls: ['./allusers-list.component.css']
})
export class AllusersListComponent implements OnInit {
  allusers: Observable<Allusers[]>;
  info: any;

  constructor(private allusersService: AllusersService, private router: Router, private token: TokenStorageService) {

  }


  ngOnInit() {
    this.reloadData();
    this.info = {
      token: this.token.getToken()

    };
  }

  reloadData() {
    this.allusers = this.allusersService.getUsersList();
  }

  deleteUser(id: number) {
    this.allusersService.deleteUser(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  userDetails(id: number) {
    this.router.navigate(['userdetails', id]);
  }

  updateUser(id: number) {
    this.router.navigate(['userupdate', id]);
  }

  gotoAddUser() {
    this.router.navigate(['/useradd']);
  }

}
