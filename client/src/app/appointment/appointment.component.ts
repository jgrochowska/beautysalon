import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {SalonService} from '../salon-service/salonService';
import {SalonServiceService} from '../salon-service/salon-service.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-calendar',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.css']
})
export class AppointmentComponent implements OnInit {
  salonServices: Observable<SalonService[]>;

  constructor(private salonServiceService: SalonServiceService, private router: Router) {
  }

  ngOnInit() {

  }
}






