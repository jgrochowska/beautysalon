import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { FlatpickrModule } from 'angularx-flatpickr';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';


import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule
} from '@angular/material';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import { HomeComponent } from './home/home.component';
import {CreateSalonServiceComponent} from './salon-service/create-salon-service/create-salon-service.component';
import {SalonServiceListComponent} from './salon-service/salon-service-list/salon-service-list.component';
import {SalonServiceDetailsComponent} from './salon-service/salon-service-details/salon-service-details.component';
import {UpdateSalonServiceComponent} from './salon-service/update-salon-service/update-salon-service.component';
import { SalonServiceCustomerListComponent } from './salon-service/salon-service-customer-list/salon-service-customer-list.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {AppointmentComponent} from './appointment/appointment.component';
import {httpInterceptorProviders} from './auth/auth-interceptor';
import {AdminComponent} from './admin/admin.component';
import {UserComponent} from './user/user.component';
import { CreateAllusersComponent } from './allusers/create-allusers/create-allusers.component';
import { AllusersDetailsComponent } from './allusers/allusers-details/allusers-details.component';
import { AllusersListComponent } from './allusers/allusers-list/allusers-list.component';
import { UpdateAllusersComponent } from './allusers/update-allusers/update-allusers.component';
import { CalendarComponent } from './calendar/calendar.component';

@NgModule({
  declarations: [
    AppComponent,
    CreateSalonServiceComponent,
    SalonServiceListComponent,
    SalonServiceDetailsComponent,
    UpdateSalonServiceComponent,
    SalonServiceCustomerListComponent,
    LoginComponent,
    RegisterComponent,
    AppointmentComponent,
    HomeComponent,
    AdminComponent,
    UserComponent,
    CreateAllusersComponent,
    AllusersDetailsComponent,
    AllusersListComponent,
    UpdateAllusersComponent,
    CalendarComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    NgbModalModule,
    FlatpickrModule.forRoot(),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    })
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent],
  exports: [CalendarComponent]
})
export class AppModule {
}
