import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {SalonService} from '../salon-service/salonService';
import {Router} from '@angular/router';
import {TokenStorageService} from '../auth/token-storage.service';
import {SalonServiceService} from '../salon-service/salon-service.service';
import {AllusersService} from '../allusers/allusers.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  // salonServices: Observable<SalonService[]>;
  info: any;

  constructor(private salonServiceService: SalonServiceService,
              private allusersService: AllusersService,
              private router: Router, private token: TokenStorageService) {

  }

  ngOnInit() {
    this.info = {
      token: this.token.getToken()

    };
  }

  gotoServicesList() {
    this.router.navigate(['/services']);
  }

  gotoUsersList() {
    this.router.navigate(['/users']);
  }
}
