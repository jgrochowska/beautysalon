import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CreateSalonServiceComponent} from './salon-service/create-salon-service/create-salon-service.component';
import {SalonServiceListComponent} from './salon-service/salon-service-list/salon-service-list.component';
import {SalonServiceDetailsComponent} from './salon-service/salon-service-details/salon-service-details.component';
import {UpdateSalonServiceComponent} from './salon-service/update-salon-service/update-salon-service.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {HomeComponent} from './home/home.component';
import {SalonServiceCustomerListComponent} from './salon-service/salon-service-customer-list/salon-service-customer-list.component';
import {AdminComponent} from './admin/admin.component';
import {AllusersListComponent} from './allusers/allusers-list/allusers-list.component';
import {UpdateAllusersComponent} from './allusers/update-allusers/update-allusers.component';
import {CreateAllusersComponent} from './allusers/create-allusers/create-allusers.component';
import {AppointmentComponent} from './appointment/appointment.component';
import {CalendarComponent} from './calendar/calendar.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },

  {path: 'services', component: SalonServiceListComponent},
  {path: 'users', component: AllusersListComponent},
  {path: 'products', component: SalonServiceCustomerListComponent},
  {path: 'add', component: CreateSalonServiceComponent},
  {path: 'useradd', component: CreateAllusersComponent},
  {path: 'update/:id', component: UpdateSalonServiceComponent},
  {path: 'userupdate/:id', component: UpdateAllusersComponent},
  {path: 'details/:id', component: SalonServiceDetailsComponent},
  {path: 'userdetails/:id', component: UpdateAllusersComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'manage', component: AdminComponent},
  {path: 'appointment', component: AppointmentComponent},
  {path: 'calendar', component: CalendarComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
