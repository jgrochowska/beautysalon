import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {SalonServiceService} from '../salon-service.service';
import {SalonService} from '../salonService';


@Component({
  selector: 'app-salon-service-list',
  templateUrl: './salon-service-customer-list.component.html',
  styleUrls: ['./salon-service-customer-list.component.css']
})
export class SalonServiceCustomerListComponent implements OnInit {
  salonServices: Observable<SalonService[]>;

  constructor(private salonServiceService: SalonServiceService, private router: Router) {

  }


  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.salonServices = this.salonServiceService.getSalonServicesCustomerList();
  }


}
