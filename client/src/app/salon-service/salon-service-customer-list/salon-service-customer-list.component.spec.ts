import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalonServiceCustomerListComponent } from './salon-service-customer-list.component';

describe('SalonServiceCustomerListComponent', () => {
  let component: SalonServiceCustomerListComponent;
  let fixture: ComponentFixture<SalonServiceCustomerListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalonServiceCustomerListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalonServiceCustomerListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
