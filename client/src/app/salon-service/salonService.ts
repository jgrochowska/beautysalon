export class SalonService {
  id: number;
  name: string;
  price: number;
  description: string;
  active: boolean;
}
