import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {SalonServiceService} from '../salon-service.service';
import {SalonService} from '../salonService';
import {TokenStorageService} from '../../auth/token-storage.service';


@Component({
  selector: 'app-salon-service-list',
  templateUrl: './salon-service-list.component.html',
  styleUrls: ['./salon-service-list.component.css']
})
export class SalonServiceListComponent implements OnInit {
  salonServices: Observable<SalonService[]>;
  info: any;

  constructor(private salonServiceService: SalonServiceService,
              private router: Router, private token: TokenStorageService) {

  }


  ngOnInit() {
    this.reloadData();
    this.info = {
      token: this.token.getToken()

    };
  }

  reloadData() {
    this.salonServices = this.salonServiceService.getSalonServicesList();
  }

  deleteSalonService(id: number) {
    this.salonServiceService.deleteSalonService(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

  salonServiceDetails(id: number) {
    this.router.navigate(['details', id]);
  }

  updateSalonService(id: number) {
    this.router.navigate(['update', id]);
  }

  gotoAddService() {
    this.router.navigate(['/add']);
  }

  
}
