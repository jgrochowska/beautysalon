import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalonServiceListComponent } from './salon-service-list.component';

describe('SalonServiceListComponent', () => {
  let component: SalonServiceListComponent;
  let fixture: ComponentFixture<SalonServiceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalonServiceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalonServiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
