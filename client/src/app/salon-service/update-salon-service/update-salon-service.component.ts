import {Component, OnInit} from '@angular/core';
import {SalonService} from '../salonService';
import {ActivatedRoute, Router} from '@angular/router';
import {SalonServiceService} from '../salon-service.service';
import {dashCaseToCamelCase} from '@angular/compiler/src/util';

@Component({
  selector: 'app-update-salon-service',
  templateUrl: './update-salon-service.component.html',
  styleUrls: ['./update-salon-service.component.css']
})
export class UpdateSalonServiceComponent implements OnInit {

  id: number;
  salonService: SalonService;

  constructor(private route: ActivatedRoute, private router: Router, private salonServiceService: SalonServiceService) {
  }

  ngOnInit() {
    this.salonService = new SalonService();

    this.id = this.route.snapshot.params['id'];

    this.salonServiceService.getSalonService(this.id)
      .subscribe(data => {
        console.log(data);
        this.salonService = data;
      }, error => console.log(error));
  }

  updateSalonService() {
    this.salonServiceService.updateSalonService(this.id, this.salonService)
      .subscribe(data => console.log(data), error => console.log(error));
    this.salonService = new SalonService();
    this.gotoList();
  }

  onSubmit() {
    this.updateSalonService();
  }

  gotoList() {
    this.router.navigate(['/services']);
  }

}
