import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalonServiceService {

  private baseUrl = 'http://localhost:8080/api/manage/services';
  private customerUrl = 'http://localhost:8080/api/customer/products';

  constructor(private http: HttpClient) {
  }

  getSalonService(id: number): Observable<any> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createSalonService(salonService: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, salonService);
  }

  updateSalonService(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteSalonService(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, {responseType: 'text'});
  }

  getSalonServicesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  getSalonServicesCustomerList(): Observable<any> {
    return this.http.get(`${this.customerUrl}`);
  }

}
