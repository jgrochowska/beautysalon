import {Component, OnInit, Input} from '@angular/core';
import {SalonService} from '../salonService';
import {SalonServiceService} from '../salon-service.service';
import {SalonServiceListComponent} from '../salon-service-list/salon-service-list.component';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-salon-service-details',
  templateUrl: './salon-service-details.component.html',
  styleUrls: ['./salon-service-details.component.css']
})
export class SalonServiceDetailsComponent implements OnInit {

  id: number;
  salonService: SalonService;

  constructor(private route: ActivatedRoute, private router: Router, private salonServiceService: SalonServiceService) {
  }

  ngOnInit() {
    this.salonService = new SalonService();

    this.id = this.route.snapshot.params['id'];

    this.salonServiceService.getSalonService(this.id)
      .subscribe(data => {
        console.log(data);
        this.salonService = data;
      }, error => console.log(error));
  }

  list() {
    this.router.navigate(['services']);
  }
}
