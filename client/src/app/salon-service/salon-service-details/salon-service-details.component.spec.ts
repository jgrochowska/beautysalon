import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalonServiceDetailsComponent } from './salon-service-details.component';

describe('SalonServiceDetailsComponent', () => {
  let component: SalonServiceDetailsComponent;
  let fixture: ComponentFixture<SalonServiceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalonServiceDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalonServiceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
