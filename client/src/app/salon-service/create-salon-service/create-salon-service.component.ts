import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {SalonService} from '../salonService';
import {SalonServiceService} from '../salon-service.service';


@Component({
  selector: 'app-create-salon-service',
  templateUrl: './create-salon-service.component.html',
  styleUrls: ['./create-salon-service.component.css']
})
export class CreateSalonServiceComponent implements OnInit {

  salonService: SalonService = new SalonService();
  submitted = false;

  constructor(private salonServiceService: SalonServiceService, private router: Router) {
  }

  ngOnInit() {
  }

  newSalonService(): void {
    this.submitted = false;
    this.salonService = new SalonService();
  }

  save() {
    this.salonServiceService.createSalonService(this.salonService)
      .subscribe(data => console.log(data), error => console.log(error));
    this.salonService = new SalonService();
    this.gotoList();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoList() {
    this.router.navigate(['/services']);
  }

}
